/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
    render() {
        const { siteConfig, language = '' } = this.props;
        const { baseUrl, docsUrl } = siteConfig;
        const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
        const langPart = `${language ? `${language}/` : ''}`;
        const docUrl = (doc) => `${baseUrl}${docsPart}${langPart}${doc}`;

        const SplashContainer = (props) => (
            <div className="homeContainer">
                <div className="homeSplashFade">
                    <div className="wrapper homeWrapper">{props.children}</div>
                </div>
            </div>
        );

        const ProjectLogo = () => <img src={`${baseUrl}img/translau-logo.png`} title="Translau logo." alt="Translau logo image." />;

        const ProjectTitle = () => (
            <h2 className="projectTitle">
                {siteConfig.title}
                <small>{siteConfig.tagline}</small>
            </h2>
        );

        const PromoSection = (props) => (
            <div className="section promoSection">
                <div className="promoRow">
                    <div className="pluginRowBlock">{props.children}</div>
                </div>
            </div>
        );

        const Button = (props) => (
            <div className="pluginWrapper buttonWrapper">
                <a className="button" href={props.href} target={props.target}>
                    {props.children}
                </a>
            </div>
        );

        return (
            <SplashContainer>
                <div className="inner">
                    <ProjectLogo />
                    <ProjectTitle siteConfig={siteConfig} />
                    <PromoSection>
                        <Button href={docUrl('getting-started')}>Installation</Button>
                        <Button href="#showcase">Showcase</Button>
                        <Button href={docUrl('examples')}>Examples</Button>
                    </PromoSection>
                </div>
            </SplashContainer>
        );
    }
}

class Index extends React.Component {
    render() {
        const { config: siteConfig, language = '' } = this.props;
        const { baseUrl } = siteConfig;

        const Block = (props) => (
            <Container padding={['bottom', 'top']} id={props.id} background={props.background}>
                <GridBlock align="center" contents={props.children} layout={props.layout} />
            </Container>
        );

        const Showcase = () => (
            <div className="productShowcaseSection paddingBottom" style={{ textAlign: 'center' }}>
                <h2 id="showcase">Showcase</h2>
                <img src={`${baseUrl}img/translau.gif`} title="Translau showcase." alt="Translau showcase gif." />
                <MarkdownBlock>Run from command line, edit in graphical interface.</MarkdownBlock>
            </div>
        );

        const Features = () => (
            <Block layout="fourColumn">
                {[
                    {
                        content: 'Simple run from your precious command line',
                        image: `${baseUrl}img/undraw_programmer_imem.svg`,
                        imageAlign: 'top',
                        title: 'CLI'
                    },
                    {
                        content: 'Edit translations in clear graphical interface',
                        image: `${baseUrl}img/undraw_wireframing_nxyi.svg`,
                        imageAlign: 'top',
                        title: 'GUI'
                    }
                ]}
            </Block>
        );

        return (
            <div>
                <HomeSplash siteConfig={siteConfig} language={language} />
                <div className="mainContainer">
                    <Features />
                    <Showcase />
                </div>
            </div>
        );
    }
}

module.exports = Index;
