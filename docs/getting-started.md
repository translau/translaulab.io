---
id: getting-started
title: Installation
---

## Fastest is the simplest

You can use **translau** simply by leveraging [npx](https://www.npmjs.com/package/npx). It will get you started in no time and all you need is [npm](https://npmjs.com) installed.

```bash
npx translau ./path/to/folder
```

> Translau is developed on Node >= 11.10.x

## Install as dependency

If you want to use **translau** in your project you can install id as a dependency or more likely as a dev dependency. Follow command below.

```bash
npm install translau
```

```bash
npm install --save-dev translau
```

Then you can use **translau** in your npm scripts. For example:

```json
{
    "scripts": {
        "i18n": "translau ./static/translations"
    }
}
```

And use then use it.

```bash
npm run i18n
```

## Want desktop experience?

**Translau** will be eventually published as an electron app once in stable version. Until then there is no alternative other than liking the corresponding [issue](https://gitlab.com/translau/translau/issues/2) or contributing.
