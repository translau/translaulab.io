---
id: creating-project
title: Creating project
---

In this guide you will learn how to create your project with translations to work best with **translau**.

## Git is our friend

You surely have encountered tool named git. If not We highly suggest to take a look at some [tutorial](https://rogerdudler.github.io/git-guide/) as you could benefit from using it. However you don't need git to work on translations. If that is the case you can jump to [folders](#structure-your-folders) right now.

But if are familiar with power of git and services working with it (such as GitLab and GitHub) we recommend to keep your translations in git repository. Simple create one using `git init` in empty directory or use existing one. You will utilize history, branches and other advantages of git and also **Translau** will integrate with it from gui.

## Structure your folders

**Translau** uses simple structure. Depending on your project size you can scale it as you wish. The basic structure is simple: folder containing **json** files in following format `<LANGUAGE_CODE>.json`. You can use whatever language code you wish (ie. [ISO-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)), full language names or your custom keys.

> You can create directories on your own or in **Translau** gui.

Example of folder structure:

```bash
i18n
|-- common
|   |-- cs.json
|   |-- en.json
|-- components
|   |-- form
|   |   |-- cs.json
|   |   |-- en.json
|   |-- menu
|   |   |-- cs.json
|   |   |-- en.json
|-- helpers
|   |-- cs.json
\   |-- en.json
```

## Let Translau do the heavy work

Now that the project is ready all that is missing is running **Translau** using one of mentioned [ways](getting-started.md) and translating some stuff!
